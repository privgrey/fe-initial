var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var minifyCSS = require('gulp-minify-css');
var notify = require('gulp-notify');
var copy = require('gulp-copy');
var sass = require('gulp-ruby-sass');
var browserSync = require('browser-sync');
var reload = browserSync.reload();

// to do list


//3.browse-sync
//4.watch reflace nofiy

// 環境變數
var env = 'prod'; // dev||prod

// 路徑變數

var appname = 'default';

var paths = {
    main: './' + appname + '/src/js/boot.js',
    css: './src/assets/css/*.css',
    destDir: './' + appname + '/build',
};

/**
 * 
 */
gulp.task('bundle-js', function() {
    
    // console.log( '\nbundle-js 跑' );

    return browserify({
        entries:[ paths.main ]
    })

    // 最優先編譯 jsx，確保後面其它 transform 運行無誤
    .transform( 'reactify' )

    // 所有檔案合併為一，並指定要生成 source map
    .bundle({debug: true})

    .on('error', function( err ){
        console.log( '[錯誤]', err );
        this.end();
        gulp.src('').pipe( notify('✖ Bunlde Failed ✖') )
    })
    
    // 利用 vinyl-source-stream 幫檔案取名字
    .pipe( source('bundle.js') )
    
    // 接著就回到 gulp 系統做剩下事
    // 這裏是直接存檔到硬碟
    .pipe( gulp.dest( paths.destDir) )


    
});



gulp.task('sass', function () {
    return gulp.src('default/src/css/app.sass')
        .pipe(sass())
        .on('error', function (err) { console.log(err.message); })
        .pipe(gulp.dest( paths.destDir));
});

/**
 * 縮短 app.css
 */
gulp.task('minify-css', function() {
  gulp.src( paths.css )
    .pipe(minifyCSS(
      {
          noAdvanced: false,
          keepBreaks:true,
          cache: true // 這是 gulp 插件獨有的
      }))
    .pipe(gulp.dest( paths.destCSS ))
});


// Start the server
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./default"
        }
    });
});

//gulp.task('bs-reload', function () {
//    browserSync.reload();
//});

gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('default', ['browser-sync'], function () {

    // add browserSync.reload to the tasks array to make
    // all browsers reload after tasks are complete.
    gulp.watch("./" + appname + "/src/css/*.sass", ['sass',browserSync.reload]);
    gulp.watch("./" + appname + "/src/css/**/*.*", ['sass',browserSync.reload]);
    gulp.watch("./" + appname + "/*.html", ['bs-reload']);
    gulp.watch("./" + appname + "/src/js/*.js", ['bundle-js',browserSync.reload]);
    gulp.watch("./" + appname + "/src/js/*.jsx", ['bundle-js',browserSync.reload]);
});